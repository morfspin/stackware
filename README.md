[![MEAN.JS Logo](http://meanjs.org/img/logo-small.png)](http://morfspin.com/)


Stackware is a full-stack JavaScript open-source solution, which provides inventory management solution for your business.

## Before You Begin
*Please contact morfspin


## Community
* Comming soon

## Contributing
We welcome pull requests from the community! Just be sure to read the [contributing](https://github.com/meanjs/mean/blob/master/CONTRIBUTING.md) doc to get started.

## Credits
Team Morfspin
## License
[The MIT License](LICENSE.md)
