'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Product Schema
 */
var ProductSchema = new Schema({
  name: {
    type: String,
    unique: 'productname already exists',
    default: '',
    required: 'Please fill Product name',
    trim: true
  },
  units:String,
  category:String,
  hasVariants:Boolean,
  variants:[{name:String, quantity:Number}],
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Product', ProductSchema);
