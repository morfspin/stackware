(function () {
  'use strict';

  // Products controller
  angular
    .module('products')
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'productResolve','CategoriesService'];

  function ProductsController ($scope, $state, $window, Authentication, product,CategoriesService) {
    var vm = this;
    vm.categories = CategoriesService.query();
    vm.authentication = Authentication;
    vm.product={};
    vm.product = product;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.querySearch=querySearch;
    vm.selectedItemChange=selectedItemChange;
    vm.searchTextChange=searchTextChange;

    if(!vm.product._id){
      vm.product.variants=[];
      vm.product.variants.push({ 'size':'' });
    }


    vm.addvariants = addvariants;
    vm.removevariants= removevariants;

    function addvariants () {
      vm.product.variants.push({ 'size':'' });

    }
    function removevariants (index) {

      vm.product.variants.splice(index,1);
    }


    function querySearch (query) {
      var results = query ? vm.categories.filter( createFilterFor(query) ) : vm.categories;

        return results;

    }

    function searchTextChange(text) {

        vm.product.category=null;

    }

    function selectedItemChange(item) {
      vm.product.category=item.name;
    }




    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(category) {
        return (category.name.indexOf(lowercaseQuery) === 0);
      };

                                      }




    // Remove existing Product
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.product.$remove($state.go('products.list'));
      }
    }

    // Save Product
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.productForm');
        return false;
      }

      // Create a new article, or update the current instance
      vm.product.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('products.list'); // should we send the User to the list or the updated Article's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Article saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Article save error!' });
      }



    }
  }
}());
