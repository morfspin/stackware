(function (app) {
  'use strict';

  // Start by defining the main module and adding the module dependencies
  angular
    .module(app.applicationModuleName, app.applicationModuleVendorDependencies);

  // Setting HTML5 Location Mode
  angular
    .module(app.applicationModuleName)
    .config(bootstrapConfig);

  bootstrapConfig.$inject = ['$compileProvider', '$locationProvider', '$httpProvider', '$logProvider','$mdThemingProvider'];

  function bootstrapConfig($compileProvider, $locationProvider, $httpProvider, $logProvider,$mdThemingProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    }).hashPrefix('!');

    $httpProvider.interceptors.push('authInterceptor');

    // Disable debug data for production environment
    // @link https://docs.angularjs.org/guide/production
    $compileProvider.debugInfoEnabled(app.applicationEnvironment !== 'production');
    $logProvider.debugEnabled(app.applicationEnvironment !== 'production');

  //  angular material palet defining
    var background = $mdThemingProvider.extendPalette('grey', {
      'default': '900',
      'A100': 'fff',
      'A200': '000',
      'A400': '000',
      'A700': '000',

      '50':'000000',
      '100':'000000',
      'contrastDefaultColor': '50',    // whether, by default, text (contrast)
      // on this palette should be dark or light
      'contrastDarkColors': '50',
      'contrastLightColors': '50'    // could also specify this if default was 'dark'

    });

    $mdThemingProvider.definePalette('background', background);

    var primary = $mdThemingProvider.extendPalette('blue-grey', {
      'default': '900',
      'A100': '000000',
      'A400': '000000',
      'A500': '000000',

      '50':'999999',
      '100':'999999',
      'contrastDefaultColor': '50',    // whether, by default, text (contrast)
      // on this palette should be dark or light
      'contrastDarkColors': '50',
      'contrastLightColors': '50'    // could also specify this if default was 'dark'

    });

    $mdThemingProvider.definePalette('primary', primary);

    var accent = $mdThemingProvider.extendPalette('blue', {
      'default': '900',
      '100':'E0BA08'

    });

    $mdThemingProvider.definePalette('accent', accent);

    //angular material theming
    $mdThemingProvider.theme('default')
      .primaryPalette('light-blue', {
        'default': 'A400', // by default use shade 400 from the pink palette for primary intentions
        'hue-1': '700', // use shade 100 for the <code>md-hue-1</code> class
        'hue-2': '500', // use shade 600 for the <code>md-hue-2</code> class
        'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
      .backgroundPalette('background',{
        'default':'A100'
      })
      .warnPalette('red')
      .accentPalette('accent', {
        'default': '900', // by default use shade 400 from the pink palette for primary intentions
        'hue-1': '500',// use shade 100 for the <code>md-hue-1</code> class
        'hue-2': '100'
      })
      .foregroundPalette['3'] = 'rgba(0,0,0,1)';

  }


  // Then define the init function for starting up the application
  angular.element(document).ready(init);

  function init() {
    // Fixing facebook bug with redirect
    if (window.location.hash && window.location.hash === '#_=_') {
      if (window.history && history.pushState) {
        window.history.pushState('', document.title, window.location.pathname);
      } else {
        // Prevent scrolling by storing the page's current scroll offset
        var scroll = {
          top: document.body.scrollTop,
          left: document.body.scrollLeft
        };
        window.location.hash = '';
        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scroll.top;
        document.body.scrollLeft = scroll.left;
      }
    }

    // Then init the app
    angular.bootstrap(document, [app.applicationModuleName]);
  }
}(ApplicationConfiguration));
