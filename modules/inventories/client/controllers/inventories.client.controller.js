(function () {
  'use strict';

  // Inventories controller
  angular
    .module('inventories')
    .controller('InventoriesController', InventoriesController);

  InventoriesController.$inject = ['$scope', '$state', '$window', 'Authentication', 'inventoryResolve','ProductsService','$filter','InventoriesService'];

  function InventoriesController ($scope, $state, $window, Authentication, inventory,ProductsService,$filter,InventoriesService) {
    var vm = this;
    vm.products = ProductsService.query();
    vm.authentication = Authentication;
    vm.inventory = {};
    vm.inventory = inventory;

    vm.inventories=InventoriesService.query();
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.addrow = addrow;
    vm.removerow= removerow;
    // product autocomplete
    vm.querySearch=querySearch;
    vm.selectedItemChange=selectedItemChange;
    vm.searchTextChange=searchTextChange;
    vm.autoLeave=autoLeave;
    //variant autocomplete
    vm.querySearchv=querySearchv;
    vm.selectedItemChangev=selectedItemChangev;
    vm.searchTextChangev=searchTextChangev;
    vm.autoLeavev=autoLeavev;

    if(!vm.inventory._id){
      vm.inventory.positive=$state.current.data.positive;
      vm.inventory.rows=[];
      vm.inventory.rows.push({ 'productname':'' });
    }
    else{
      angular.forEach(vm.inventory.rows, function (value, key) {
        value.searchText=value.productname;
        value.searchTextv=value.variant;


      });
    }



    function addrow () {
      vm.inventory.rows.push({ 'productname':'' });

    }
    function removerow (index) {

      vm.inventory.rows.splice(index,1);
    }

          //product autocomplete

          function autoLeave (row,index) {
             if(row.productname)
             {}
             else{vm.form.inventoryForm['product' + index].$error.required=true;}
          }

          function querySearch (query) {
            var results = query ? vm.products.filter( createFilterFor(query) ) : vm.products;

            return results;

          }

          function searchTextChange(row,index) {
            row.p=null;
            row.productname= null;
            row.productid= null;
            row.variant= null;
            row.searchTextv= null;
            row.bal= null;

          }

          function selectedItemChange(item,row) {

            //check if product is already selected
            //if variants no need of above and stock
            if(item) {
              if (!item.hasVariants) {

                if ($filter('filter')(vm.inventory.rows, {
                    productname: item.name
                  }).length > 0) {
                  return;
                }
                //populate stock a

                row.bal = 0;
                angular.forEach(vm.inventories, function (value, key) {
                  vm.filteredItems = [];
                  vm.filteredItems = $filter('filter')(value.rows, {
                    productname: item.name
                  });
                  if (vm.filteredItems.length > 0) {

                    if (vm.filteredItems[0].add) {
                      row.bal = row.bal + vm.filteredItems[0].add;
                    }
                    if (vm.filteredItems[0].subtract) {

                      row.bal = row.bal - vm.filteredItems[0].subtract;
                    }

                  }

                });
              }

            row.form={};
            row.form.productname={};
            row.form.productname.valid=true;
            row.productname=item.name;
            row.productid=item._id;

            }


          }

          function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(product) {
              return (product.name.indexOf(lowercaseQuery) === 0);
            };

          }


          //product variant autocomplete
          function querySearchv (query,row) {

            var results = query ? $filter('filter')(vm.products, {
              name: row.productname
            })[0].variants.filter( createFilterForv(query) ) : $filter('filter')(vm.products, {
              name: row.productname
            })[0].variants;

            return results;

          }

          function autoLeavev (row,index) {
            if(!row.variant)
            {vm.form.inventoryForm['variant' + index].$error.required=true;}
          }

          function searchTextChangev(row) {

            row.variant=null;
            row.bal=null;


          }

          function selectedItemChangev(item,row) {
            if(item) {
              if ($filter('filter')(vm.inventory.rows, {
                  productname: item.name,variant:item.name
                }).length > 0) {
                return;
              }
              row.variant = item.name;
              //populate stock
              row.bal = 0;
              angular.forEach(vm.inventories, function (value, key) {
                vm.filteredItems = [];
                vm.filteredItems = $filter('filter')(value.rows, {
                  productname: row.productname, variant: item.name
                });
                if (vm.filteredItems.length > 0) {

                  if (vm.filteredItems[0].add) {
                    row.bal = row.bal + vm.filteredItems[0].add;
                  }
                  if (vm.filteredItems[0].subtract) {

                    row.bal = row.bal - vm.filteredItems[0].subtract;
                  }

                }

              });
            }
          }

          function createFilterForv(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(product) {
              return (product.name.indexOf(lowercaseQuery) === 0);
            };

          }













    // Remove existing Inventory
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.inventory.$remove($state.go('inventories.list'));
      }
    }

    // Save Inventory
    function save(isValid) {
      // console.log(vm.form.inventoryForm);
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.inventoryForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.inventory._id) {
        vm.inventory.$update(successCallback, errorCallback);
      } else {
        vm.inventory.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('inventories.view', {
          inventoryId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
