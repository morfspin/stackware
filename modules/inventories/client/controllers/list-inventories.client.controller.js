(function () {
  'use strict';

  angular
    .module('inventories')
    .controller('InventoriesListController', InventoriesListController);

  InventoriesListController.$inject = ['InventoriesService','$filter','ProductsService'];

  function InventoriesListController(InventoriesService,$filter,ProductsService) {
    var vm = this;

    vm.inventories = InventoriesService.query();

    vm.buildPager = buildPager;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.pageChanged = pageChanged;

    ProductsService.query(function (data) {
      vm.products = data;
      vm.buildPager(vm.products);
    });

    function buildPager(data) {
      vm.pagedItems = [];
      vm.itemsPerPage = 15;
      vm.currentPage = 1;
      vm.figureOutItemsToDisplay(data);
    }

    function figureOutItemsToDisplay(data) {
      vm.filteredItems = $filter('filter')(data, {
        $: vm.search
      });
      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
    }

    function pageChanged(data) {
      vm.figureOutItemsToDisplay(data);
    }





  }
}());
