(function () {
  'use strict';

  // Products controller
  angular
    .module('inventories')
    .controller('ProductstockController', ProductstockController);

  ProductstockController.$inject = ['$scope','productResolve','CategoriesService','InventoriesService','$filter'];

  function ProductstockController ($scope, productResolve, CategoriesService,InventoriesService,$filter) {
    var vm = this;
    vm.product = productResolve;
    vm.bal=0;
    vm.data=[];
    vm.inventories = InventoriesService.query(function() {
      if(!vm.product.hasVariants) {

          angular.forEach(vm.inventories, function (value, key) {
          vm.filteredItems = [];

          vm.filteredItems = $filter('filter')(value.rows, {
            $: vm.product.name
          });
          if (vm.filteredItems.length > 0) {
            var obj = {};
            if (vm.filteredItems[0].add) {
              obj.add = vm.filteredItems[0].add;
              vm.bal =+ obj.add;
            }
            if (vm.filteredItems[0].subtract) {
              obj.subtract = vm.filteredItems[0].subtract;
              vm.bal =- obj.subtract;
            }

            obj.user = value.user.displayName;
            obj.name = value.name;
            obj.created = value.created;
            obj._id = value._id;
            obj.bal = vm.bal;

            vm.data.push(obj);

          }
          else {
          }
        });
           }
    });


    // VARIANT autocomplete
    vm.querySearch=querySearch;
    vm.selectedItemChange=selectedItemChange;
    vm.searchTextChange=searchTextChange;


    //product autocomplete
    function querySearch (query) {
      var results = query ? vm.product.variants.filter( createFilterFor(query) ) : vm.product.variants;

      return results;

    }

    function searchTextChange(row) {


    }

    function selectedItemChange(item) {
      vm.variant=item.name;
      vm.data=[];
      vm.bal = 0;
      angular.forEach(vm.inventories, function (value, key) {
        vm.filteredItems = [];

        vm.filteredItems = $filter('filter')(value.rows, {
          productname: vm.product.name, variant: vm.variant
        });
        if (vm.filteredItems.length > 0) {
          var obj = {};
          if (vm.filteredItems[0].add) {
            obj.add = vm.filteredItems[0].add;
            vm.bal =vm.bal+ obj.add;
          }
          if (vm.filteredItems[0].subtract) {
            obj.subtract = vm.filteredItems[0].subtract;
            vm.bal =vm.bal- obj.subtract;
          }

          obj.user = value.user.displayName;
          obj.name = value.name;
          obj.created = value.created;
          obj._id = value._id;
          obj.bal = vm.bal;

          vm.data.push(obj);

        }
        else {
        }
      });


    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(p) {
        return (p.name.indexOf(lowercaseQuery) === 0);
      };

    }



    function figureOutItemsToDisplay() {



    }





  }
}());
