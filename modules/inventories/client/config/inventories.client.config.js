(function () {
  'use strict';

  angular
    .module('inventories')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Inventories',
      state: 'inventories',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'inventories', {
      title: 'List Inventories',
      state: 'inventories.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'inventories', {
      title: 'Add Stock',
      state: 'inventories.createadd',
      roles: ['user']
    });
    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'inventories', {
      title: 'Remove Stock',
      state: 'inventories.createremove',
      roles: ['user']
    });
    menuService.addSubMenuItem('topbar', 'inventories', {
      title: 'Product Stock',
      state: 'inventories.productstocklist',
      roles: ['user']
    });
  }
}());
