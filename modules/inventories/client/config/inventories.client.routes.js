(function () {
  'use strict';

  angular
    .module('inventories')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('inventories', {
        abstract: true,
        url: '/inventories',
        template: '<ui-view/>'
      })
      .state('inventories.list', {
        url: '',
        templateUrl: '/modules/inventories/client/views/list-inventories.client.view.html',
        controller: 'InventoriesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Inventories List'
        }
      })
      .state('inventories.productstocklist', {
        url: '/productstock',
        templateUrl: '/modules/inventories/client/views/product-inventories.client.view.html',
        controller: 'InventoriesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Inventories List'
        }
      })
      .state('inventories.productstock', {
        url: '/:productId/view',
        templateUrl: '/modules/inventories/client/views/productstock.client.view.html',
        controller: 'ProductstockController',
        controllerAs: 'vm',
        resolve: {
          productResolve: getProduct
        },data: {
          roles: ['user', 'admin']

        }
      })

      .state('inventories.createadd', {
        url: '/addstock',
        templateUrl: '/modules/inventories/client/views/form-inventory.client.view.html',
        controller: 'InventoriesController',
        controllerAs: 'vm',
        resolve: {
          inventoryResolve: newInventory
        },
        data: {
          roles: ['user', 'admin'],
          positive:true,
          pageTitle: 'Inventories Add Stock'
        }
      })
      .state('inventories.createremove', {
        url: '/removestock',
        templateUrl: '/modules/inventories/client/views/form-inventory.client.view.html',
        controller: 'InventoriesController',
        controllerAs: 'vm',
        resolve: {
          inventoryResolve: newInventory
        },
        data: {
          roles: ['user', 'admin'],
          positive:false,
          pageTitle: 'Inventories Remove Stock'
        }
      })
      .state('inventories.edit', {
        url: '/:inventoryId/edit',
        templateUrl: '/modules/inventories/client/views/form-inventory.client.view.html',
        controller: 'InventoriesController',
        controllerAs: 'vm',
        resolve: {
          inventoryResolve: getInventory
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Inventory {{ inventoryResolve.name }}'
        }
      })
      .state('inventories.view', {
        url: '/:inventoryId',
        templateUrl: '/modules/inventories/client/views/view-inventory.client.view.html',
        controller: 'InventoriesController',
        controllerAs: 'vm',
        resolve: {
          inventoryResolve: getInventory
        },
        data: {
          pageTitle: 'Inventory {{ inventoryResolve.name }}'
        }
      });
  }

  getInventory.$inject = ['$stateParams', 'InventoriesService'];


  function getInventory($stateParams, InventoriesService) {
    return InventoriesService.get({
      inventoryId: $stateParams.inventoryId
    }).$promise;
  }

  newInventory.$inject = ['InventoriesService'];

  function newInventory(InventoriesService) {
    return new InventoriesService();
  }

  getProduct.$inject = ['$stateParams', 'ProductsService'];

  function getProduct($stateParams, ProductsService) {
    return ProductsService.get({
      productId: $stateParams.productId
    }).$promise;
  }





}());
