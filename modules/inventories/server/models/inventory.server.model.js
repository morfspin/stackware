'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Inventory Schema
 */
var InventorySchema = new Schema({
   name:String,
   positive:Boolean,
   rows:[{
          productid: {
                      type: Schema.ObjectId,
                      ref: 'Product'
                      },
           productname:{type:String,required:"product name missing"},
           variant:String,
           subtract:Number,
           add:Number
          }],
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Inventory', InventorySchema);
